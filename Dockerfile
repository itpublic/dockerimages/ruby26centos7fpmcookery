FROM centos/ruby-26-centos7
USER root

RUN curl -sL https://rpm.nodesource.com/setup_14.x | bash -
RUN yum -y install nodejs
RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
RUN yum -y install yarn
RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install \
    ca-certificates \
    ansible \
    autoconf \
    automake \
    bison \
    gcc \
    gcc-c++ \
    git \
    libffi-devel \
    libtool \
    make \
    openssh-clients \
    openssl \
    openssl-devel \
    readline-devel \
    rpm-build \
    rsync \
    ruby \
    sqlite-devel \
    vim \
    wget \
    which \
    zlib-devel \
 && yum clean all

RUN gem install -N fpm-cookery

SHELL ["/bin/bash", "-lc"]
CMD ["/bin/bash", "-l"]

